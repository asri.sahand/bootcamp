from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _

import bleach
from bootcamp.activities.models import Activity
from bootcamp.relationship.models import Relation


@python_2_unicode_compatible
class Feed(models.Model):
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    post = models.TextField(max_length=255)
    parent = models.ForeignKey('Feed', null=True, blank=True)
    likes = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)
    reported = models.BooleanField(default=False)
    retweets = models.IntegerField(default=0)

    class Meta:
        verbose_name = _('Feed')
        verbose_name_plural = _('Feeds')
        ordering = ('-date',)

    def __str__(self):
        return self.post

    @staticmethod
    def get_feeds(from_feed=None, user=None):
        if user is None:
            if from_feed is not None:
                feeds = Feed.objects.filter(parent=None, id__lte=from_feed, reported=False)
            else:
                feeds = Feed.objects.filter(parent=None, reported=False)
        else:
            muted_ids = Relation.objects.filter(me=user, type='mute').values_list('you__id', flat=True)
            followings = Relation.objects.filter(me=user, type='follow').values_list('you', flat=True)
            followings = list(followings) + [user]
            if from_feed is not None:
                feeds = Feed.objects.filter(parent=None, id__lte=from_feed, reported=False, user__in=followings).exclude(user__in=muted_ids)
            else:
                feeds = Feed.objects.filter(parent=None, reported=False, user__in=followings).exclude(user__in=muted_ids)
        return feeds

    @staticmethod
    def get_feeds_after(feed, user=None):
        if user is None:
            feeds = Feed.objects.filter(parent=None, id__gt=feed)
        else:
            muted_ids = Relation.objects.filter(me=user, type='mute').values_list('you__id', flat=True)
            followings = Relation.objects.filter(me=user, type='follow').values_list('you', flat=True)
            followings = list(followings) + [user]
            feeds = Feed.objects.filter(parent=None, id__gt=feed, user__in=followings).exclude(user__in=muted_ids)
        return feeds

    def get_comments(self):
        return Feed.objects.filter(parent=self).order_by('date')

    def calculate_likes(self):
        likes = Activity.objects.filter(activity_type=Activity.LIKE,
                                        feed=self.pk).count()
        self.likes = likes
        self.save()
        return self.likes

    def get_likes(self):
        likes = Activity.objects.filter(activity_type=Activity.LIKE,
                                        feed=self.pk)
        return likes

    def get_likers(self):
        likes = self.get_likes()
        likers = []
        for like in likes:
            likers.append(like.user)
        return likers

    def get_retweeters(self):
        retweets = self.get_retweets()
        retweeters = []
        for retweet in retweets:
            retweeters.append(retweet.user)
        return retweeters

    def calculate_retweets(self):
        retweets = Activity.objects.filter(activity_type=Activity.RETWEET,
                                           feed=self.pk).count()
        self.retweets = retweets
        self.save()
        return self.retweets

    def get_retweets(self):
        retweets = Activity.objects.filter(activity_type=Activity.RETWEET,
                                           feed=self.pk)
        return retweets

    def calculate_comments(self):
        self.comments = Feed.objects.filter(parent=self).count()
        self.save()
        return self.comments

    def comment(self, user, post):
        feed_comment = Feed(user=user, post=post, parent=self)
        feed_comment.save()
        self.comments = Feed.objects.filter(parent=self).count()
        self.save()
        return feed_comment

    def linkfy_post(self):
        return bleach.linkify(escape(self.post))


class Report(models.Model):
    reporter = models.ForeignKey(User)
    feed = models.ForeignKey(Feed)

    class Meta:
        unique_together = ('reporter', 'feed',)