# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-07 08:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('feeds', '0005_auto_20180104_1439'),
        ('authentication', '0002_profile_blue_tick'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='pinned_post',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='feeds.Feed'),
        ),
    ]
