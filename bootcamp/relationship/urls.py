from django.conf.urls import url

from bootcamp.relationship import views

urlpatterns = [
    url(r'^$', views.relation, name='relation'),
]
