from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

class Relation(models.Model):
    me = models.ForeignKey(User, related_name="me")
    you = models.ForeignKey(User, related_name="you")
    type = models.TextField(max_length=20)

    @staticmethod
    def is_muted(me, you):
        return Relation.objects.filter(me=me, you=you, type='mute').exists()

    @staticmethod
    def is_followed(me, you):
        return Relation.objects.filter(me=me, you=you, type='follow').exists()
