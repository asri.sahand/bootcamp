from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse

from bootcamp.decorators import ajax_required
from bootcamp.relationship.models import Relation


@login_required
@ajax_required
def relation(request):
    user_id = request.POST['user_id']
    type = request.POST['type']
    you = User.objects.get(pk=user_id)
    relation = Relation.objects.filter(me=request.user, you=you, type=type)
    if relation:
        relation.delete()
    else:
        relation = Relation(me=request.user, you=you, type=type)
        relation.save()
    return HttpResponse(status=200)
