/**
 * Created by ali on 12/29/2017 AD.
 */

$(function () {

  $("#mute").click(function () {
    var profile = $(".profile");
    var user_id = $(profile).attr("user-id");
    var csrf = $(profile).attr("csrf");
    var type = 'mute'
    $.ajax({
      url: '/relation/',
      data: {
        'user_id': user_id,
        'type': type,
        'csrfmiddlewaretoken': csrf
      },
      type: 'post',
      cache: false,
      success: function (data) {
        button = $("#mute");
        if(button.hasClass("btn-mute")){
          button.removeClass("btn-mute");
          button.addClass("btn-unmute");
          button.text("Unmute");
        } else {
          button.removeClass("btn-unmute");
          button.addClass("btn-mute");
          button.text("Mute");
        }

      }
    });
  });

});

$(function () {

  $("#follow").click(function () {
    var profile = $(".profile");
    var user_id = $(profile).attr("user-id");
    var csrf = $(profile).attr("csrf");
    var type = 'follow'
    $.ajax({
      url: '/relation/',
      data: {
        'user_id': user_id,
        'type': type,
        'csrfmiddlewaretoken': csrf
      },
      type: 'post',
      cache: false,
      success: function (data) {
        button = $("#follow");
        if(button.hasClass("btn-follow")){
          button.removeClass("btn-follow");
          button.addClass("btn-unfollow");
          button.text("Unfollow");
        } else {
          button.removeClass("btn-unfollow");
          button.addClass("btn-follow");
          button.text("Follow");
        }

      }
    });
  });

});
